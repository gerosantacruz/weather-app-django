from django.shortcuts import render, redirect
from django.conf import settings
import requests
from .models import City
from .forms import CityForm

# Create your views here.

def index(request):
    
    
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=' + settings.API_KEY #convert teh information of the api in Json
    err_mesg = ''
    messaage = ''
    messaage_color =''

    if request.method == 'POST':
        form = CityForm(request.POST)
        
        if form.is_valid():
            new_city = form.cleaned_data['name']
            exist_city_count = City.objects.filter(name=new_city).count()

            if exist_city_count == 0:
                req = requests.get(url.format(new_city)).json()
                if req['cod'] == 200:
                    form.save()
                else:
                    err_mesg = 'City does not exist'
            else:
                err_mesg = 'City already exist in the Database'
        if err_mesg:
            messaage=err_mesg
            messaage_color = 'is-danger'
        else:
            messaage = 'City added succesfully'
            messaage_color = 'is-success'

  
    form = CityForm()
    
    cities = City.objects.all() #Return all the cities in the data.

    weather_data = []

    
    for city in cities:
        
        
        r = requests.get(url.format(city)).json()
        
        city_weather = {
            'city' : city.name,
            'temperature' : r['main']['temp'],
            'description' : r['weather'][0]['description'],
            'icon' : r['weather'][0]['icon'],
        }

        weather_data.append(city_weather)
        
    

    context = {
        'weather_data': weather_data,
        'form':form,
        'message': messaage,
        'message_color': messaage_color
        }
        
    return render(request, 'weather/index.html', context)

def delete_city(request, city_name):
    City.objects.get(name=city_name).delete()
    return redirect('home')


    # Render the information to the view
    